"""Test HttpClient functionality with a minimal flask test server."""

import functools
import pickle
import time

import flask
import pytest
import requests

import fw_http_client

# HttpClient requires name/version - use test/1.0 throughout tests
client_info = dict(client_name="test", client_version="1.0")
HttpClient = functools.partial(fw_http_client.HttpClient, **client_info)


def test_client_with_default_client_name_and_version():
    client = fw_http_client.HttpClient()
    expected_useragent = f"fw-http-client/{fw_http_client.__version__}"
    assert client.headers["User-Agent"] == expected_useragent


def test_client_with_empty_baseurl_uses_request_url(http_testserver):
    http_testserver.add_response("/path", {})
    client = HttpClient()
    client.get(f"{http_testserver.url}/path")
    assert http_testserver.request_log == ["GET /path"]


def test_client_with_custom_client_auth_string_uses_headers(http_testserver):
    http_testserver.add_response("/path", {})
    client = HttpClient(auth="custom")
    client.get(f"{http_testserver.url}/path")
    assert http_testserver.request_log == ["GET /path"]
    assert http_testserver.request_map["GET /path"].headers.Authorization == "custom"


def test_client_with_custom_request_auth_string_uses_headers(http_testserver):
    http_testserver.add_response("/path", {})
    client = HttpClient()
    client.get(f"{http_testserver.url}/path", auth="custom")
    assert http_testserver.request_log == ["GET /path"]
    assert http_testserver.request_map["GET /path"].headers.Authorization == "custom"


@pytest.fixture()
def client(http_testserver):
    return HttpClient(baseurl=http_testserver.url)


def test_client_prefixes_request_url_with_baseurl(http_testserver, client):
    http_testserver.add_response("/path", {})
    client.get("/path")
    assert http_testserver.request_log == ["GET /path"]


def test_client_returns_response_on_raw_request(http_testserver, client):
    http_testserver.add_response("/text", "text")
    response = client.get("/text", raw=True)
    assert isinstance(response, requests.Response)
    assert response.ok
    assert response.content == b"text"


def test_client_loads_json_to_attrdict(http_testserver, client):
    http_testserver.add_response("/json", {"foo": "bar"})
    response = client.get("/json")
    assert response.foo == "bar"


def test_client_loads_attrdicts_recursively(http_testserver, client):
    http_testserver.add_response("/recurse", [{"foo": {"bar": "baz"}}])
    response = client.get("/recurse")
    assert response[0].foo.bar == "baz"


def test_client_returns_none_on_empty_response(http_testserver, client):
    http_testserver.add_response("/empty", "")
    response = client.get("/empty")
    assert response is None


def test_client_supports_pickle(http_testserver, client):
    http_testserver.add_response("/pickle", "")
    client = pickle.loads(pickle.dumps(client))
    assert client.get("/pickle") is None


def test_client_raises_on_invalid_json_response(http_testserver, client):
    http_testserver.add_response("/invalid", "a" * 2000)
    expected_error = (
        f"GET {http_testserver.url}/invalid - invalid JSON response: {'a' * 997}..."
    )
    with pytest.raises(fw_http_client.RequestException, match=expected_error):
        client.get("/invalid")


def test_client_raises_on_client_error(http_testserver, client):
    http_testserver.add_response("/client-error", "foo", method="POST", status=400)
    expected_error = (
        rf"POST {http_testserver.url}/client-error\?param=1 - 400 BAD REQUEST"
        f"\nResponse: foo"
    )
    with pytest.raises(fw_http_client.ClientError, match=expected_error):
        client.post(
            "/client-error",
            headers={"Authorization": "Bearer foo", "X-Auth": "barbaz"},
            params={"param": 1},
            json={"bar": "baz"},
        )


def test_client_error_message_with_redirect(http_testserver, client):
    redirect = functools.partial(flask.redirect, f"{http_testserver.url}/location")
    http_testserver.add_callback("/redirect", redirect)
    http_testserver.add_response("/location", status=400)
    expected_error = (
        f"GET {http_testserver.url}/redirect - 302 FOUND\n"
        f"GET {http_testserver.url}/location - 400 BAD REQUEST"
    )
    with pytest.raises(fw_http_client.ClientError, match=expected_error):
        client.get("/redirect")


def test_client_error_message_with_json_response(http_testserver, client):
    err_json = {"message": "foo"}
    http_testserver.add_response("/client-error", err_json, method="POST", status=400)
    expected_error = (
        f"POST {http_testserver.url}/client-error - 400 BAD REQUEST\nResponse: foo"
    )
    with pytest.raises(fw_http_client.ClientError, match=expected_error):
        client.post("/client-error", "")


def test_error_proxies_response_attrs(http_testserver, client):
    http_testserver.add_response("/client-error", {"detail": "foo"}, status=400)
    with pytest.raises(fw_http_client.ClientError) as exc:
        client.get("/client-error")
    error = exc.value
    assert error.status_code == 400
    assert error.reason == "BAD REQUEST"
    assert error.json().detail == "foo"
    with pytest.raises(AttributeError):
        getattr(error, "bar")


def test_client_raises_on_client_error_when_streaming(http_testserver, client):
    http_testserver.add_response("/stream", "", status=400)
    with pytest.raises(fw_http_client.ClientError):
        client.get("/stream", stream=True)


def test_client_does_not_raise_on_client_error_when_raw(http_testserver, client):
    http_testserver.add_response("/raw", "some client error", status=400)
    response = client.get("/raw", raw=True)
    assert isinstance(response, requests.Response)
    assert response.status_code == 400
    assert response.content == b"some client error"


def test_client_retries_on_server_error(http_testserver, client):
    status = iter([503, 200])
    http_testserver.add_callback("/server-error-flaky", lambda: ({}, next(status)))
    response = client.get("/server-error-flaky")
    assert response == {}


def test_client_retries_max_n_times(http_testserver):
    status = iter([503, 200])
    http_testserver.add_callback("/server-error-flaky", lambda: ({}, next(status)))
    client = HttpClient(baseurl=http_testserver.url, retry_total=0)
    expected_error = (
        f"GET {http_testserver.url}/server-error-flaky - 503 SERVICE UNAVAILABLE"
    )
    with pytest.raises(fw_http_client.ServerError, match=expected_error):
        client.get("/server-error-flaky")


def test_client_raises_on_name_resolution_error():
    client = HttpClient(baseurl="http://localhost.test", retry_total=0)
    expected_error = r"GET http://localhost.test/path - \[Errno -[23]\] .*"
    with pytest.raises(fw_http_client.ConnectionError, match=expected_error):
        client.get("/path")


def test_client_raises_on_connection_abort():
    client = HttpClient(baseurl="http://localhost:9955", retry_total=0)
    expected_error = (
        r"GET http://localhost:9955/path - ("
        r"\[Errno 111\] Connection refused|"
        r"Remote end closed connection without response)"
    )
    with pytest.raises(fw_http_client.ConnectionError, match=expected_error):
        client.get("/path")


def test_client_raises_on_timeout(http_testserver):
    http_testserver.add_callback("/timeout", lambda: time.sleep(1))
    client = HttpClient(baseurl=http_testserver.url, read_timeout=0.01, retry_total=0)
    expected_error = (
        rf"GET {http_testserver.url}/timeout - Read timed out. \(read timeout=0.01\)"
    )
    with pytest.raises(fw_http_client.ConnectionError, match=expected_error):
        client.get("/timeout")


@pytest.mark.parametrize(
    "useragent, parsed",
    [
        (
            "app/1.0",
            {"name": "app", "version": "1.0"},
        ),
        (
            "app/1.0 (k1:v1)",
            {"name": "app", "version": "1.0", "k1": "v1"},
        ),
        (
            "app/1.0 (k1:v1; k2:v2)",
            {"name": "app", "version": "1.0", "k1": "v1", "k2": "v2"},
        ),
    ],
)
def test_useragent(useragent, parsed):
    assert fw_http_client.load_useragent(useragent) == parsed
    assert fw_http_client.dump_useragent(**parsed) == useragent


def test_response_iter_lines(http_testserver, client):
    http_testserver.add_response("/lines", "line\n" * 10)
    response = client.get("/lines", stream=True)
    assert list(response.iter_lines()) == [b"line"] * 10


def test_response_iter_jsonl(http_testserver, client):
    http_testserver.add_response("/jsonl", '{"foo":"bar"}\n' * 10)
    response = client.get("/jsonl", stream=True)
    assert list(response.iter_jsonl()) == [{"foo": "bar"}] * 10


MULTIPART_HEADER = {"content-type": "multipart/mixed;boundary=deadbeefdeadbeef"}
MULTIPART_BODY = b"""
ignored preamble
--deadbeefdeadbeef
content-disposition: form-data; name="field1"

implicit text/plain
--deadbeefdeadbeef
content-type: application/json

{"foo":"bar"}
--deadbeefdeadbeef
content-type: text/xml

<foo>bar</foo>
--deadbeefdeadbeef--
ignored epilogue
"""

MULTIPART_BODY = MULTIPART_BODY.replace(b"\n", b"\r\n")
EXPECTED_PARTS = [b"implicit text/plain", b'{"foo":"bar"}', b"<foo>bar</foo>"]


def test_response_iter_parts_yields_multipart_contents(http_testserver, client):
    http_testserver.add_response("/multipart", MULTIPART_BODY, headers=MULTIPART_HEADER)
    response = client.get("/multipart", stream=True)
    assert [part.content for part in response.iter_parts()] == EXPECTED_PARTS


def test_response_iter_parts_with_starting_delimiter(http_testserver, client):
    body = MULTIPART_BODY.replace(b"\r\nignored preamble\r\n", b"")
    http_testserver.add_response("/multipart", body, headers=MULTIPART_HEADER)
    response = client.get("/multipart", stream=True)
    assert [part.content for part in response.iter_parts()] == EXPECTED_PARTS


def test_response_iter_parts_yields_full_msg_if_no_boundary(http_testserver, client):
    headers = {"content-type": "multipart"}
    http_testserver.add_response("/multipart", "content", headers=headers)
    response = client.get("/multipart", stream=True)
    assert [part.content for part in response.iter_parts()] == [b"content"]


def test_response_iter_parts_raises_on_invalid_content_type(http_testserver, client):
    http_testserver.add_response("/multipart", "content")
    response = client.get("/multipart", stream=True)
    with pytest.raises(ValueError):
        next(response.iter_parts())


def test_response_iter_parts_raises_on_missing_double_crlf(http_testserver, client):
    invalid_body = b"\r\n--deadbeefdeadbeef\r\nNO-CRLF\r\n--deadbeefdeadbeef"
    http_testserver.add_response("/multipart", invalid_body, headers=MULTIPART_HEADER)
    response = client.get("/multipart", stream=True)
    with pytest.raises(ValueError):
        next(response.iter_parts())


def test_response_iter_parts_warns_without_closing_delimiter(http_testserver, client):
    invalid_body = b"\r\n--deadbeefdeadbeef\r\n\r\nPART\r\n--deadbeefdeadbeefNO--"
    http_testserver.add_response("/multipart", invalid_body, headers=MULTIPART_HEADER)
    response = client.get("/multipart", stream=True)
    with pytest.warns(UserWarning):
        list(response.iter_parts())


SSE_HEADER = {"content-type": "text/event-stream"}
SSE_BODY = b"""
: comment only in block - no event

: event w/ id and multiple data lines showcasing whitespace handling
id: 1
data
data:
data:a
data: b
data:  c

: event w/ type, retry and : in data value
event: evt
data: foo:bar
retry: 1000

: empty id - resets the last event id w/o event
id

: invalid id/field/retry and incomplete block - all ignored
id: ignored\0 - id ignored if contains null-byte
foo: ignored - field ignored if not id|event|data|retry
retry: ignored - retry ignored if not isdigit
data: ignored - blocks only fire events if there's an empty line after
""".lstrip()

Evt = fw_http_client.client.Event
EXPECTED_EVENTS = [
    Evt(id="1", data="\n\na\nb\n c"),
    Evt(type="evt", data="foo:bar", retry=1000),
]


@pytest.mark.parametrize("eol", [b"\r\n", b"\n", b"\r"])
def test_response_iter_events_yields_sse_events(http_testserver, client, eol):
    body = SSE_BODY.replace(b"\n", eol)
    http_testserver.add_response("/events", body, headers=SSE_HEADER)
    response = client.get("/events", stream=True)
    assert list(response.iter_events(chunk_size=1)) == EXPECTED_EVENTS


def test_response_iter_events_raises_on_invalid_content_type(http_testserver, client):
    http_testserver.add_response("/events", SSE_BODY)
    response = client.get("/events", stream=True)
    with pytest.raises(ValueError):
        next(response.iter_events(chunk_size=1))


def test_error_message_truncate(http_testserver, client):
    http_testserver.add_response("/client-error", "a" * 2000, method="POST", status=400)
    expected_error = (
        rf"POST {http_testserver.url}/client-error\?param=1 - 400 BAD REQUEST"
        f"\nResponse: {'a' * 997}..."
    )
    with pytest.raises(fw_http_client.ClientError, match=expected_error):
        client.post(
            "/client-error",
            headers={"Authorization": "Bearer foo", "X-Auth": "barbaz"},
            params={"param": 1},
            json={"bar": "baz"},
        )


def test_error_message_truncate_binary(http_testserver, client):
    msg = ("a" * 2000).encode("utf-16")
    http_testserver.add_response("/client-error", msg, method="POST", status=400)
    expected_error = (
        rf"POST {http_testserver.url}/client-error?param=1 - 400 BAD REQUEST"
        f"\nResponse: {str(msg)[:97]}..."
    )
    with pytest.raises(fw_http_client.ClientError) as excinfo:
        client.post(
            "/client-error",
            headers={"Authorization": "Bearer foo", "X-Auth": "barbaz"},
            params={"param": 1},
            json={"bar": "baz"},
        )
    assert str(excinfo.value) == expected_error
