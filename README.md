# fw-http-client

Prod-ready HTTP client with timeouts and retries by default.

## Installation

Add as a `poetry` dependency to your project:

```bash
poetry add fw-http-client
```

## Usage

```python
from fw_http_client import HttpClient

client = HttpClient(client_name="my-app", client_version="1.0")
data = client.get("https://httpbin.org/json")
assert data.slideshow.title == "Sample Slide Show"
```

## Development

Install the project using `poetry` and enable `pre-commit`:

```bash
poetry install
pre-commit install
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
